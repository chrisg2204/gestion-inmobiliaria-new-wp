<?php

/**
* Plugin Name: Gestion Inmobiliaria New
* Plugin URI: http://www.ejme.com.ve
* Description: Plugin para gestionar inmuebles.
* Version: 1.0.0
* Author: Edwin Mogollón <edwin.jme@hotmail.com>, Christian Giménez <christiang15@hotmail.com>
* Author URI: http://www.ejme.com.ve
**/

function gestionarInmueblesView() {
	require_once (dirname( __FILE__ )."/public_html/views/gestionar-inmuebles.php");
}

function gestionarInmueblesInternacionalView() {
	require_once (dirname( __FILE__ )."/public_html/views/gestionar-inmuebles-internacional.php");
}

function gestionarDestacadosView() {
	require_once (dirname( __FILE__ )."/public_html/views/gestionar-destacado.php");
}

function gestionarTipoDePropiedadView() {
	require_once (dirname( __FILE__ )."/public_html/views/gestionar-tipos-propiedad.php");
}

function gestionarVendedorView() {
	require_once (dirname( __FILE__ )."/public_html/views/gestionar-vendedor.php");
}

function mapView() {
	require_once (dirname( __FILE__ )."/public_html/views/mapview.php");
}

function gestionInmobiliatiaInit() {
	add_menu_page("Gestionar", "Gestionar", "0", "gestionarInmueblesView", "", "dashicons-store", 101);
	add_submenu_page("gestionarInmueblesView", "Inmuebles", "Inmuebles", "0", "gestionarInmueblesView", "gestionarInmueblesView");
	add_submenu_page("gestionarInmueblesView", "Inmueble Internacional", "Inmueble Internacional", "0", "gestionarInmueblesInternacionalView", "gestionarInmueblesInternacionalView");
	add_submenu_page("gestionarInmueblesView", "Destacado", "Destacado", "0", "gestionarDestacadosView", "gestionarDestacadosView");
	add_submenu_page("gestionarInmueblesView", "Tipos Propiedad", "Tipos Propiedad", "0", "gestionarTipoDePropiedadView", "gestionarTipoDePropiedadView");
	add_submenu_page("gestionarInmueblesView", "Vendedor", "Vendedor", "0", "gestionarVendedorView", "gestionarVendedorView");
	add_submenu_page("gestionarInmueblesView", "Visor de mapa", "Visor de mapa", "0", "mapView", "mapView");
}

add_action("admin_menu", "gestionInmobiliatiaInit");
add_action("wp_ajax_ajaxConversionGestionInmobiliaria", "ajaxConversionGestionInmobiliaria");
add_action("wp_ajax_nopriv_ajaxConversionGestionInmobiliaria", "ajaxConversionGestionInmobiliaria");


require_once(dirname( __FILE__ ).'/php/util/Util.php');
require_once(dirname( __FILE__ ).'/php/business/BusinessProperty.php');

function ajaxConversionGestionInmobiliaria() {
	if (isset($_GET["task"])) {
		BusinessProperty::getTask($_GET["task"]);
	} else {
		print_r(Util::response(404, "Task is null or empty", true));
	};

	die();
}
