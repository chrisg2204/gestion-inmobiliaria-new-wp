function sendFormEditProperty(id) {
	$('#buttonEditProperty').click(function(e) {
		var formData = $('#formEditProperty').serialize();

		$.ajax({
			url: '/wordpress/wp-admin/admin-ajax.php?task=UPDATE_PROPERTY' + '&idProperty=' + id + '&action=ajaxConversionGestionInmobiliaria',
			type: 'POST',
			data: formData,
			cache: false,
			beforeSend: function() {
				$('#buttonEditProperty').text(' Editando...').attr('disabled', 'disabled').prepend('<i class="fa fa-spinner fa-spin"></i>');
			},
			success: function(data) {
				console.log(data);
				var parseResult = JSON.parse(data);
				if (parseResult.status === 200) {
					toastr.success(parseResult.data);
				} else if (parseResult.status === 400) {
					toastr.warning(parseResult.data);
				} else if (parseResult.status === 500) {
					toastr.error(parseResult.data);
				}
			},
			error: function(err) {
				console.log(err);
				var parseErr = JSON.parse(err);
				toastr.error(parseErr.data);
			},
			complete: function() {
				$('#buttonEditProperty').text(' Editar').removeAttr('disabled').prepend('<i class="fa fa-pencil fa-lg"></i>');
			}
		});

		e.preventDefault();
	});
}