class MapView {

	constructor(longitude, latitude) {
		var lon = (longitude === null) ? LONGITUDE : longitude;
		var lat = (latitude === null) ? LATITUDE : latitude;

		var map = L.map('mapid').setView([lon, lat], ZOOMINIT);

		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
			maxZoom: MAXZOOM,
		}).addTo(map);

		L.marker([lon, lat]).addTo(map)
			.bindPopup(`Coordenadas LON : ${lon}, LAT : ${lat}`)
			.openPopup();
	}

}