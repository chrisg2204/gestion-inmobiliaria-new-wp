class GridVendedor {

	constructor() {
		this.path = 'http://127.0.0.1:8000';
		this.users = '/api/users'

	}

	grid() {
		const self = this;

		$('#tableVendedor').bootstrapTable({
			method: 'GET',
			url: self.path + self.users,
			striped: true,
			columns: [{
				field: 'id',
				title: 'id',
				align: 'center'
			}, {
				title: 'Nombres',
				align: 'center',
				formatter: this.formatterName
			}, {
				field: 'email',
				title: 'Correo',
				align: 'center'
			}, {
				field: 'phone',
				title: 'Telefono',
				align: 'center'
			}, {
				title: 'Acciones',
				align: 'center',
				formatter: this.formatterEvent,
				events: {
					'click .edit': function(e, value, row, index) {
						self.viewEditVendedor(row);
						self.editVendedor(e, row);
					},
					'click .remove': function(e, value, row, index) {
						self.deleteCarousel(e, row);
					}
				}
			}]
		});
	}

	formatterName(value, row, index) {
		return `${row.firstname} ${row.lastname}`;
	}

	formatterEvent(value, row, index) {
		var ret = ['<a class="btn btn-warning btn-xs edit" href="javascript:void(0)" title="Editar Propiedad">', '<i class="fa fa-pencil fa-lg"></i>', '</a>  ',
			'<a class="btn btn-danger btn-xs remove" href="javascript:void(0)" title="Eliminar Propiedad">', '<i id=delete' + row.idVendedor + ' class="fa fa-remove fa-lg"></i>', '</a>'
		];
		return ret.join('');
	}

	viewEditVendedor(row) {
		$('#firstname').val(row.firstname);
		$('#lastname').val(row.lastname);
		$('#email').val(row.email);
		$('#nationality').val(row.nationality);
		$('#phone').val(row.phone);
		$('#phone_optional').val(row.phone_optional);
		this.toggleModal('#modalEditVendedor', 'show');
	}

	editVendedor(e, row) {
		const self = this;
		let parentEvent = e;

		$('#buttonEditVendedor').unbind('click').click((e) => {
			let serializedData = $('#formEditVendedor').serialize();
			serializedData = serializedData.replace(/&?[^=]+=&|&[^=]+=$/g, '');

			$.ajax({
					url: self.path + self.users + '/' + row.id,
					type: 'PUT',
					data: serializedData,
					beforeSend: () => {
						$('#buttonEditVendedor').text(' Editando...').attr('disabled', 'disabled').prepend('<i class="fa fa-spinner fa-spin"></i>');
					}
				})
				.done((data, textStatus, jqXHR) => {
					if (jqXHR.status === 200) {
						toastr.success(jqXHR.responseJSON.message);
					} else {
						console.log(jqXHR);
					}
				})
				.fail((jqXHR, textStatus, errorThrown) => {
					if (jqXHR.status === 400) {
						let validationsErr = jqXHR.responseJSON.error;
						if (validationsErr.firstname) {
							toastr.error(validationsErr.firstname[0]);
							$('#group-firstname').addClass('has-error has-feedback');
						}
						if (validationsErr.lastname) {
							toastr.error(validationsErr.lastname[0]);
							$('#group-lastname').addClass('has-error has-feedback');
						}
						if (validationsErr.nationality) {
							toastr.error(validationsErr.nationality[0]);
							$('#group-nationality').addClass('has-error has-feedback');
						}
						if (validationsErr.phone) {
							toastr.error(validationsErr.phone[0]);
							$('#group-phone').addClass('has-error has-feedback');
						}
						if (validationsErr.phone_optional) {
							toastr.error(validationsErr.phone_optional[0]);
							$('#group-phone-optional').addClass('has-error has-feedback');
						}
					} else if (jqXHR.status === 404) {
						toastr.warning(jqXHR.responseJSON.message)
					} else if (jqXHR.status === 500) {
						toastr.error(jqXHR.responseJSON.message);
					} else {
						console.log(jqXHR);
					}
				})
				.always(() => {
					$('#buttonEditVendedor').text(' Editar').removeAttr('disabled').prepend('<i class="fa fa-pencil fa-lg"></i>');
				});
			e.preventDefault();
		});
	}

	deleteCarousel(e, row) {
		const self = this;

		let confirmr = confirm('Esta seguro de eliminar este Vendedor ?');
		if (confirmr === true) {
			$.ajax({
					url: self.path + self.users + '/' + row.id,
					type: 'DELETE',
					cache: false,
					beforeSend: () => {
						$('#delete' + row.idVendedor).removeClass('class="fa fa-remove fa-lg"').prepend('<i class="fa fa-spinner fa-spin"></i>');
					}
				})
				.done((data, textStatus, jqXHR) => {
					if (jqXHR.status === 200) {
						toastr.success(jqXHR.responseJSON.message);
					} else {
						console.log(jqXHR);
					}
				})
				.fail((jqXHR, textStatus, errorThrown) => {
					if (jqXHR.status === 404) {
						toastr.warning(jqXHR.responseJSON.message);
					} else if (jqXHR.status === 500) {
						toastr.error(jqXHR.responseJSON.message);
					} else {
						console.log(jqXHR);
					}
				})
				.always(() => {
					self.refreshGrid('#tableVendedor');
				});

			e.preventDefault();

		} else {
			return false;
		}
	}

	toggleModal(modalId, event) {
		$(modalId).modal(event);
	}

	cleanForm(formId) {
		$(formId)[0].reset();
	}

	refreshGrid(gridId) {
		$(gridId).bootstrapTable('refresh');
	}

}