function sendFormAddProperty() {
	$('#buttonAddProperty').click(function(e) {
		var formData = new FormData($('#propertyForm')[0]);
		formData.append('action', 'ajaxConversionGestionInmobiliaria');

		$.ajax({
			url: '/wordpress/wp-admin/admin-ajax.php?' + 'task=' + 'ADD_PROPERTY',
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('#buttonAddProperty').text(' Enviando...').attr('disabled', 'disabled').prepend('<i class="fa fa-spinner fa-spin"></i>');
			},
			success: function(data) {
				console.log(data);
				var parseData = JSON.parse(data);
				if (parseData.status === 200) {
					toastr.success(parseData.data);
					cleanForm('#propertyForm');
					refreshGrid('#tableProperties');
					refreshGrid('#tableResources')
				} else if (parseData.status === 400) {
					toastr.warning(parseData.data);
				} else if (parseData.status === 404) {
					toastr.warning(parseData.data);
				} else {
					toastr.error(parseData.data);;
				}
			},
			error: function(err) {
				console.log(err);
				var parseErr = JSON.parse(err);
				toastr.error(parseErr.data);
			},
			complete: function() {
				$('#buttonAddProperty').text(' Enviar').removeAttr('disabled').prepend('<i class="fa fa-check fa-lg"></i>');
			}
		});

		e.preventDefault();
	});
}

function refreshGrid(gridId) {
	$(gridId).bootstrapTable('refresh');
}

function cleanForm(formId) {
	$(formId)[0].reset();
}