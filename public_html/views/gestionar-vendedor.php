<?php
/**
 * Comment Management Screen
 *
 * @package WordPress
 * @subpackage Administration
 */

/** Load WordPress Bootstrap */

?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Gestionar Vendedor</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/bootstrap/app.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/toastr/toastr.min.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/font-awesome/font-awesome.min.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/table/bootstrap-table.min.css" /> 
   </head>
   <body style="background-color:#F1F1F1">
      <div class="container">
         <div class="row" style="margin-top:1%">
            <div class="col-md-12">
               <div class="panel panel-default">
                  <div class="panel-heading text-center">
                     <b class="panel-title">Gestionar Vendedor</b>
                  </div>
                  <div class="panel-body">
                     <ul class="nav nav-pills">
                        <li class="active">
                           <a href="#listar" data-toggle="tab">Listar Vendedor</a>
                        </li>
                     </ul>
                     <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="listar">

                        	<table 
                              id="tableVendedor"
                              name="tableVendedor"
                              data-show-refresh="true"
                              data-page-list="[5, 10, 20, 50, 100, 200]"
                              data-side-pagination="server"
                              data-pagination="true"
                              data-show-columns="true"
                              data-show-toggle="true"
                              data-search="true">
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
   <!-- Modal Editar -->
   <div class="modal fade" id="modalEditVendedor" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalVendedorAriaL" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button name="btnCancel" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title text-center">Editar Vendedor</h4>
            </div>
            <div class="modal-body">
               <form id="formEditVendedor" name="formEditVendedor" class="form-hotizontal">
                  <fieldset>
                     <div id="group-firsname" class="form-group col-md-6" style="margin-top:1%">
                        <label class="control-label" for="firstname">Nombres</label> 
                        <input id="firstname" name="firstname" type="text" placeholder="Nombres" class="form-control input-md" onchange="onChangeFirstname()" required>
                     </div>
                     <div id="group-lastname" class="form-group col-md-6" style="margin-top:1%">
                        <label class="control-label" for="lastname">Apellidos</label> 
                        <input id="lastname" name="lastname" type="text" placeholder="Apellidos" class="form-control input-md" onchange="onChangeLastname()" required>
                     </div>
                     <div id="group-email" class="form-group col-md-6" style="margin-top:1%">
                        <label class="control-label" for="email">Correo</label> 
                        <input id="email" name="email" type="email" placeholder="Correo" class="form-control input-md" disabled>
                     </div>
                     <div id="group-nationality" class="form-group col-md-6" style="margin-top:1%">
                        <label class="control-label" for="nationality">Nacionalidad</label> 
                        <input id="nationality" name="nationality" type="text" placeholder="Nacionalidad" onchange="onChangeNationality()" class="form-control input-md" required>
                     </div>
                     <div id="group-phone" class="form-group col-md-6" style="margin-top:1%">
                        <label class="control-label" for="phone">Teléfono</label> 
                        <input id="phone" name="phone" type="text" placeholder="Teléfono" class="form-control input-md" onchange="onChangePhone()" required>
                     </div>
                     <div id="group-phone-optional" class="form-group col-md-6" style="margin-top:1%">
                        <label class="control-label" for="phone_optional">Teléfono opcional</label> 
                        <input id="phone_optional" name="phone_optional" type="text" placeholder="Teléfono opcional" class="form-control input-md" onchange="onChangePhoneOptional()" required>
                     </div>
                  </fieldset>
               </form>
            </div>
            <div class="modal-footer">
               <button id="buttonEditVendedor" name="buttonEditVendedor" class="btn btn-warning btn-lg">Editar <i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button>
               <button name="btnSalir" data-dismiss="modal" class="btn btn-default btn-lg"><span class="fa fa-close fa-lg"></span> Salir</button>
            </div>
         </div>
      </div>
   </div>
   <!-- Modal Editar -->
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/bootstrap/app.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/toastr/toastr.min.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/table/bootstrap-table.min.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/table/bootstrap-table-locale-all.min.js"></script>

   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/gestionar-vendedor.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/addVendedor.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/gridVendedor.js"></script>
   <script type="text/javascript">
      function onChangeFirstname() {
         $('#firstname').parent().removeClass('has-error has-feedback');
      }

      function onChangeLastname() {
         $('#lastname').parent().removeClass('has-error has-feedback');
      }

      function onChangeNationality() {
         $('#nationality').parent().removeClass('has-error has-feedback');
      }

      function onChangePhone() {
         $('#phone').parent().removeClass('has-error has-feedback');
      }

      function onChangePhoneOptional() {
         $('#phone_optional').parent().removeClass('has-error has-feedback');
      }
   </script>

</html>