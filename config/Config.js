/**
 * Leaflet config.
 */

// País.
const COUNTRY = 'Spain';

// Longitud.
const LONGITUDE = 40.416775;

// Latitud.
const LATITUDE = -3.703790;

// Zoom inicial.
const ZOOMINIT = 12;

// Zoom maximo.
const MAXZOOM = 18;