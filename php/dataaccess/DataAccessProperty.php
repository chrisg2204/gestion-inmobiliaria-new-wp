<?php

require_once(dirname( __FILE__ ).'/../datasource/DataSource.php');
require_once(dirname( __FILE__ ).'/../datatransfer/DataTransferProperty.php');
require_once(dirname( __FILE__ ).'/../datatransfer/DataTransferDestacado.php');
require_once(dirname( __FILE__ ).'/../datatransfer/DataTransferResource.php');
require_once(dirname( __FILE__ ).'/../datatransfer/DataTransferSeller.php');
require_once(dirname( __FILE__ ).'/../datatransfer/DataTransferTypeProperty.php');
require_once(dirname( __FILE__ ).'/../interface/InterfaceProperty.php');
require_once(dirname( __FILE__ ).'/../util/Util.php');

class DataAccessProperty implements InterfaceProperty {

    public function addProperty(DataTransferProperty $dtoProperty) {
        $prepared = true;
        $reqInsert = array();
        $insertFields = "";
        $typeParams = array();
        $insertParams = "";
        $response = "";
        $dataSource = new DataSource();

        $title = $dtoProperty->getTitle();
        $typeProperty = $dtoProperty->getPublicationType();
        $price = $dtoProperty->getPrice();
        $address = $dtoProperty->getAddress();
        $departament = $dtoProperty->getDepartament();
        $province = $dtoProperty->getProvince();
        $district = $dtoProperty->getDistrict();
        $area = $dtoProperty->getArea();
        $latitude = $dtoProperty->getLatitude();
        $longitude = $dtoProperty->getLongitude();
        $enterprice = $dtoProperty->getEnterprice();
        $video = $dtoProperty->getVideo();
        $description = $dtoProperty->getDescription();
        $currency = $dtoProperty->getCurrency();
        $image = $dtoProperty->getImage();
        $idSeller = $dtoProperty->getIdSeller();
        $propertyType = $dtoProperty->getPropertyType();

        if (empty($title) && $title === "") {
            $response = Util::response(400, "Campo Título requerido.", true);
            $prepared = false;
        } else {
            $insertFields .= "title, ";
            array_push($reqInsert, $title);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if (empty($typeProperty) && $typeProperty === 0) {
            $response = Util::response(400, "Seleccione tipo de inmueble.", true);
            $prepared = false;
        } else {
            $insertFields .= "id_publication_type, ";
            array_push($reqInsert, $typeProperty);
            array_push($typeParams, PDO::PARAM_INT);
        }
        if ($currency) {
            $insertFields .= "currency, ";
            array_push($reqInsert, $currency);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if (empty($price) && $price === "") {
            $response = Util::response(400, "Campo Precio requerido.", true);
            $prepared = false;
        } else {
            if (!is_numeric($price)) {
                $response = Util::response(400, "Campo Precio invalido.", true);
                $prepared = false;
            } else {
                $insertFields .= "price, ";
                array_push($reqInsert, $price);
                array_push($typeParams, PDO::PARAM_STR);
            }
        }
        if (empty($address) && $address === "") {
            $response = Util::response(400, "Campo Dirección requerida.", true);
            $prepared = false;
        } else {
            $insertFields .= "address, ";
            array_push($reqInsert, $address);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if ($departament === 0) {
            $response = Util::response(400, "Seleccione Departamento.", true);
            $prepared = false;
        } else {
            $insertFields .= "departament, ";
            array_push($reqInsert, $departament);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if ($province === 0) {
            $response = Util::response(400, "Seleccione Provincia.", true);
            $prepared = false;
        } else {
            $insertFields .= "province, ";
            array_push($reqInsert, $province);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if ($district === 0) {
            $response = Util::response(400, "Seleccione Distrito.", true);
            $prepared = false;
        } else {
            $insertFields .= "district, ";
            array_push($reqInsert, $district);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if (empty($area) && $area === "") {
            $response = Util::response(400, "Campo Area requerido.", true);
            $prepared = false;
        } else {
            $insertFields .= "area, ";
            array_push($reqInsert, $area);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if (!empty($longitude) && $longitude !== "") {
            if (!is_numeric($longitude)) {
                $response = Util::response(400, "Campo Longitud invalido.", true);
                $prepared = false;
            } else {
                $insertFields .= "longitude, ";
                array_push($reqInsert, $longitude);
                array_push($typeParams, PDO::PARAM_STR);
            }
        }
        if (!empty($latitude) && $latitude !== "") {
            if (!is_numeric($latitude)) {
                $response = Util::response(400, "Campo Latitud invalido.", true);
                $prepared = false;
            } else {
                $insertFields .= "latitude, ";
                array_push($reqInsert, $latitude);
                array_push($typeParams, PDO::PARAM_STR);
            }
        }
        if (!empty($video) && $video !== "") {
            $insertFields .= "url_video, ";
            array_push($reqInsert, $video);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if (!empty($enterprice) && $enterprice !== "") {
            $insertFields .= "enterprice_name, ";
            array_push($reqInsert, $enterprice);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if (empty($description) && $description === "") {
            $response = Util::response(400, "Campo Descripción requerido.", true);
            $prepared = false;
        } else {
            $insertFields .= "description, ";
            array_push($reqInsert, $description);
            array_push($typeParams, PDO::PARAM_STR);
        }
        if ($idSeller == 0) {
            $response = Util::response(400, "Seleccione Vendedor.", true);
            $prepared = false;
        } else {
            $insertFields .= "id_seller, ";
            array_push($reqInsert, $idSeller);
            array_push($typeParams, PDO::PARAM_INT);
        }
        if ($propertyType == 0) {
            $response = Util::response(400, "Seleccione Tipo de Propiedad.", true);
            $prepared = false;
        } else {
            $insertFields .= "id_property_type, ";
            array_push($reqInsert, $propertyType);
            array_push($typeParams, PDO::PARAM_INT);
        }
        if ($image["size"] == 0 && $image["error"] == 4) {
            $response = Util::response(400, "No ha insertado imagen aun.", true);
            $prepared = false;
        } else {
            $verifyMimetypeImage = Util::verifyMimetype($image["name"]);
            if ($verifyMimetypeImage["success"] == true) {
                if (!function_exists("wp_handle_upload")) {
                    require_once(ABSPATH . "wp-admin/includes/file.php");
                }

                $image["name"] = $verifyMimetypeImage["newName"];
                $uploadedfile = $image;
                $upload_overrides = array("test_form" => false);
                $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
            } else {
                $response = Util::response(400, "Formato de imagen invalido.", true);
                $prepared = false;
            }
        }

        if ($prepared == true) {
            for ($i = 0; $i < count($reqInsert); $i++) {
                $insertParams .= "?, ";
            }
            $preparedInsertFields = substr($insertFields, 0, -2);
            $preparedInsertParams = substr($insertParams, 0, -2);
            
            $sqlAddProperty = "INSERT INTO `".DATABASE_NAME."`.`wp_properties` ($preparedInsertFields) VALUES ($preparedInsertParams)";

            if ($dataSource->execQuery($sqlAddProperty, $typeParams, $reqInsert)) {

                $lastId = $dataSource->getLastId();
                $getYear = date("Y");
                $getMonth = date("m");

                $reqResource = array($lastId, 1, $image["name"], "/wordpress/wp-content/uploads/".$getYear."/".$getMonth."/".$image["name"]);
                $sqlResource = "INSERT INTO `".DATABASE_NAME."`.`wp_property_resource` (id_property, id_type_resource, name, url_path)
                                VALUES (?, ?, ?, ?)";
                $typesParams = array(PDO::PARAM_INT, PDO::PARAM_INT, PDO::PARAM_STR, PDO::PARAM_STR);

                if ($dataSource->execQuery($sqlResource, $typesParams, $reqResource)) {
                    $response = Util::response(200, "Inmueble registrado satisfactoriamente.", false);
                } else {
                    $response = Util::response(500, "Error al registran recurso.", true);
                }
            } else {
                $response = Util::response(500, "Error al registran inmueble.", true);
            }

            print_r($response);
        } else {
            print_r($response);
        }
    }

    public function findProperties(DataTransferProperty $dtoProperty) {
        $condition = "";
        $dataSource = new DataSource();

        $id = $dtoProperty->getId();
        $title = $dtoProperty->getTitle();
        $price = $dtoProperty->getPrice();
        $searchType = $dtoProperty->getSearchType();
        $searchValue = $dtoProperty->getSearchValue();
        $order = $dtoProperty->getOrder();
        $limit = $dtoProperty->getLimit();
        $offset = $dtoProperty->getOffset();
        $international = $dtoProperty->getInternational();

        if ($searchType === "all" && $searchValue !== "") {
            $condition .= "a.id LIKE '%$searchValue%' OR ";
            $condition .= "a.title LIKE '%$searchValue%' OR ";
            $condition .= "a.price LIKE '%$searchValue%' OR ";
            $condition .= "a.created LIKE '%$searchValue%' OR ";
        } else if ($searchType === "particular") {
            if ($id !== "") {
                $condition .= "a.id LIKE '%$id%' OR ";
            }
            if ($title !== "") {
                $condition .= "a.title LIKE '%$title%' OR ";
            }
            if ($price !== "") {
                $condition .= "a.price LIKE '%$price%' OR ";   
            }
        }

        if ($international === true) {
            if ($condition === "") {
                $condition = "1 = 1 AND a.departament = '' AND a.province = '' AND a.district = ''";
            } else {
                $condition = substr($condition, 0, -3);
                $condition .= " AND a.departament = '' AND a.province = '' AND a.district = ''";
            }
        } else {
            if ($condition === "") {
                $condition = "1 = 1 AND a.departament != '' AND a.province != '' AND a.district != ''";
            } else {
                $condition = substr($condition, 0, -3);
                $condition .= " AND a.departament != '' AND a.province != '' AND a.district != ''";
            }
        }

        $sqlFindAllProperties = "SELECT SQL_CALC_FOUND_ROWS
                                    a.id AS `idPropiedad`,
                                    a.id_publication_type AS `idTipoDePublicacion`,
                                    a.id_seller AS `idVendedor`,
                                    a.id_property_type AS `idTipoPropiedad`,
                                    a.title AS `titulo`,
                                    a.address AS `direccion`,
                                    a.departament AS `departamento`,
                                    a.province AS `provincia`,
                                    a.district AS `distrito`,
                                    a.area AS `area`,
                                    a.latitude AS `latitud`,
                                    a.longitude AS `longitud`,
                                    a.description AS `descripcion`,
                                    a.url_video AS `urlVideo`,
                                    a.enterprice_name AS `empresa`,
                                    CONCAT(a.price, ' ', a.currency) AS `precio`,
                                    DATE_FORMAT(a.created, '%d-%m-%Y') AS `fechaDeCreacion`
                                FROM `".DATABASE_NAME."`.`wp_properties` a
                                    WHERE $condition
                                        ORDER BY a.id $order LIMIT $limit OFFSET $offset";

        $execQuery = $dataSource->execSelectAndCountQuery($sqlFindAllProperties);
        print_r($execQuery);
    }

    public function selectProperties() {
        $dataSource = new DataSource();
        $destacadosRegister = $this->destacadoForCompare();
        $condition = '';
        if ($destacadosRegister == "()") {
            $condition = "1 = 1";
        } else {
            $condition = "a.id NOT IN $destacadosRegister";
        }
        $sqlSelectProperties = "SELECT
                                    a.id AS `idPropiedad`,
                                    a.title AS `titulo`
                                FROM `".DATABASE_NAME."`.`wp_properties` a
                                    LEFT OUTER JOIN `".DATABASE_NAME."`.`wp_destacado_property` b
                                        ON a.id = b.id_property
                                    LEFT OUTER JOIN `".DATABASE_NAME."`.`wp_destacado` c
                                        ON b.id_destacado = c.id
                                WHERE $condition";
        print_r($dataSource->execSelectQuery($sqlSelectProperties));
    }

    public function destacadoForCompare() {
        $dataSource = new DataSource();
        $sqlDestacadoForCompare = "SELECT
                                        a.id_property
                                    FROM `".DATABASE_NAME."`.`wp_destacado_property` a
                                        INNER JOIN `".DATABASE_NAME."`.`wp_destacado` b
                                            ON a.id_destacado = b.id";

        $queryResult = json_decode($dataSource->execSelectQuery($sqlDestacadoForCompare), true);
        $arr = array();
        for ($i = 0; $i < count($queryResult); $i++) {
            array_push($arr, $queryResult[$i]["id_property"]);
        }
        $separateResult = implode(",", $arr);

        return "(".$separateResult.")";
    }

    public function addDestacado(DataTransferProperty $dtoProperty, DataTransferDestacado $dtoDestacado) {
        $prepared = true;
        $response = "";
        $lastIdDestacado = '';
        $dataSource = new DataSource();

        $idProperty = $dtoProperty->getId();
        $position = $dtoDestacado->getPosition();

        if (!is_numeric($idProperty)) {
            $response = Util::response(400, "Id inmueble no es numerico.", true);
            $prepared = false;
        }
        if ($idProperty == 0) {
            $response = Util::response(400, "Seleccione un inmueble para continuar.", true);
            $prepared = false;
        }
        if (!is_numeric($position)) {
            $response = Util::response(400, "Posición no es numerico.", true);
            $prepared = false;
        }
        if ($position == 0) {
            $response = Util::response(400, "Seleccione una posición.", true);
            $prepared = false;
        }
        if (count(Util::decodeJSON($this->findOneProperty($dtoProperty))) !== 1) {
            $response = Util::response(400, "Inmueble no encontrado para asociar.", true);
            $prepared = false;
        }
       
        if ($prepared === true) {
            $destacadoExist = Util::decodeJSON($this->findOnePositionOnDestacado($dtoDestacado));
            if (count($destacadoExist) === 1) {
                $destacadoUpdate = $destacadoExist[0]["idDestacado"];
                $reqDestacadoUpdate = array($idProperty, $destacadoUpdate);
                $typesDestacadoUpdate = "ii";
                $sqlDestacadoUpdate = "UPDATE `".DATABASE_NAME."`.`wp_destacado_property` SET id_property = ? WHERE id = ?";

                if ($dataSource->execQuery($sqlDestacadoUpdate, $typesDestacadoUpdate, $reqDestacadoUpdate)) {
                    $response = Util::response(200, "Destacado registrado con exito.", false);
                } else {
                    $response = Util::response(500, "Error al registrar destacado.", true);
                }
            } else {
                $reqDestacado = array($position);
                $sqlDestacado = "INSERT INTO `".DATABASE_NAME."`.`wp_destacado` (position) VALUES (?)";
                $typeDestacado = array(PDO::PARAM_STR);

                if ($dataSource->execQuery($sqlDestacado, $typeDestacado, $reqDestacado)) {
                    $lastIdDestacado = $dataSource->getLastId();
                    $reqDestacadoProperty = array($lastIdDestacado, $idProperty);
                    $sqlDestacadoProperty = "INSERT INTO `".DATABASE_NAME."`.`wp_destacado_property` (id_destacado, id_property) VALUES (?, ?)";
                    $typeDestacadoProperty = array(PDO::PARAM_INT, PDO::PARAM_INT);

                    if ($dataSource->execQuery($sqlDestacadoProperty, $typeDestacadoProperty, $reqDestacadoProperty)) {
                        $response = Util::response(200, "Destacado registrado con exito.", false);
                    } else {
                        $response = Util::response(500, "Error al registrar destacado.", true);
                    }
                } else {
                    $response = Util::response(500, "Error al registrar destacado.", true);
                }
            }

            print_r($response);
        } else {

            print_r($response);
        }
    }

    public function findOneProperty(DataTransferProperty $dtoProperty) {
        $dataSource = new DataSource();
        $id = $dtoProperty->getId();
        $condition = "a.id = $id";

        $sqlFindOneProperty = "SELECT
                                    a.id AS `idPropiedad`,
                                    a.id_publication_type AS `idTipoDePublicacion`,
                                    a.title AS `titulo`,
                                    a.address AS `direccion`,
                                    a.departament AS `departamento`,
                                    a.province AS `provincia`,
                                    a.district AS `distrito`,
                                    a.area AS `area`,
                                    a.latitude AS `latitud`,
                                    a.longitude AS `longitud`,
                                    a.enterprice_name AS `empresa`,
                                    a.description AS `descripcion`,
                                    a.url_video AS `urlVideo`, 
                                    CONCAT(a.price, ' ', a.currency) AS `precio`,
                                    DATE_FORMAT(a.created, '%d-%m-%Y') AS `fechaDeCreacion`
                                FROM `".DATABASE_NAME."`.`wp_properties` a
                                    WHERE $condition LIMIT 1";

        $execQuery = $dataSource->execSelectQuery($sqlFindOneProperty);
        return $execQuery;
    }

     public function findOnePositionOnDestacado(DataTransferDestacado $dtoDestacado) {
        $dataSource = new DataSource();
        $position = $dtoDestacado->getPosition();
        $condition = "a.position = $position";

        $sqlFindOnePositionOnDestacado = "SELECT
                                            a.id AS `idDestacado`,
                                            DATE_FORMAT(a.created, '%d-%m-%Y') AS `fechaDeCreacion`,
                                            a.position AS `posicion`
                                          FROM `".DATABASE_NAME."`.`wp_destacado` a
                                            INNER JOIN `".DATABASE_NAME."`.`wp_destacado_property` b
                                                ON a.id = b.`id_destacado` WHERE $condition LIMIT 1";

        $execQuery = $dataSource->execSelectQuery($sqlFindOnePositionOnDestacado);
        return $execQuery;
    }

    public function findResources(DataTransferResource $dtoResource) {
        $condition = "";
        $dataSource = new DataSource();

        $idProperty = $dtoResource->getIdProperty();
        $created = $dtoResource->getCreatedDate();
        $searchType = $dtoResource->getSearchType();
        $searchValue = $dtoResource->getSearchValue();
        $order = $dtoResource->getOrder();
        $limit = $dtoResource->getLimit();
        $offset = $dtoResource->getOffset();
        $international = $dtoResource->getInternational();

        if ($searchType === "all" && $searchValue !== "") {
            $condition .= "b.title LIKE '%$searchValue%' OR ";
            $condition .= "a.created LIKE '%$searchValue%' OR ";
        } else if ($searchType === "particular") {
            if ($idProperty !== "") {
                $condition .= "a.id_property = $idProperty   ";
            }
        }

         if ($international === "true") {
            if ($condition === "") {
                $condition = "1 = 1 AND b.departament = '' AND b.province = '' AND b.district = ''";
            } else {
                $condition = substr($condition, 0, -3);
                $condition .= " AND b.departament = '' AND b.province = '' AND b.district = ''";
            }
        } else {
            if ($condition === "") {
                $condition = "1 = 1 AND b.departament != '' AND b.province != '' AND b.district != ''";
            } else {
                $condition = substr($condition, 0, -3);
                $condition .= " AND b.departament != '' AND b.province != '' AND b.district != ''";
            }
        }

        $sqlFindAllResources = "SELECT
                                    SQL_CALC_FOUND_ROWS a.id AS `idRecurso`,
                                    a.id_property AS `idPropiedad`,
                                    a.id_type_resource AS `idTipoDeRecurso`,
                                    a.name AS `nombreDeRecurso`,
                                    a.url_path AS `urlRecurso`,
                                    b.title AS `titulo`,
                                    b.departament AS `departamento`,
                                    b.province AS `provincia`,
                                    b.district AS `distrito`,
                                    DATE_FORMAT(a.created, '%d-%m-%Y') AS`fechaDeCreacion`
                                FROM `".DATABASE_NAME."`.`wp_property_resource` a
                                    INNER JOIN `".DATABASE_NAME."`.`wp_properties` b 
                                        ON b.id = a.id_property WHERE $condition ORDER BY a.id $order LIMIT $limit OFFSET $offset";


        $execQuery = $dataSource->execSelectAndCountQuery($sqlFindAllResources);
        print_r($execQuery);
    }

    // TODO: Eliminar imagen de recurso del server.
    public function deleteOneResource(DataTransferResource $dtoResource) {
        $response = "";
        $prepared = true;
        $dataSource = new DataSource();

        $idResource = $dtoResource->getId();
        $idProperty = $dtoResource->getIdProperty();

        if (!is_numeric($idResource)) {
            $response = Util::response(400, "Id de recurso invalido.", true);
            $prepared = false;
        }
        if (!is_numeric($idProperty)) {
            $response = Util::response(400, "Id de propiedad invalido.", true);
            $prepared = false;
        }
        if (count(Util::decodeJSON($this->findOneResource($dtoResource))) !== 1) {
            $response = Util::response(400, "Recurso no encontrado para eliminar.", true);
            $prepared = false;
        }
        if ($prepared === true) {
            $req = array($idResource, $idProperty);
            $sqlDelete = "DELETE
                                    FROM `".DATABASE_NAME."`.`wp_property_resource`
                                        WHERE id = ?
                                            AND id_property = ?";
            $types = array(PDO::PARAM_INT, PDO::PARAM_INT);
            if ($dataSource->execQuery($sqlDelete, $types, $req)) {
                $response = Util::response(200, "Recurso eliminado satisfactoriamente.", false);
            } else {
                $response = Util::response(500, "Error al eliminar recurso.", true);
            }
            print_r($response);
        } else {
            print_r($response);
        }
    }

    public function addResource(DataTransferResource $dtoResource) {
        $prepared = true;
        $response = "";
        $dataSource = new DataSource();

        $idProperty = $dtoResource->getIdProperty();
        $file = $dtoResource->getFile();

        if (!is_numeric($idProperty)) {
            $response = Util::response(400, "Id propiedad no es numerico.", true);
            $prepared = false;
        }
        if ($file["size"] === 0 && $file["error"] === 4) {
            $response = Util::response(400, "No ha insertado recurso aun.", true);
            $prepared = false;
        } else {
            $verifyMimetype = Util::verifyMimetype($file["name"]);
            if ($verifyMimetype["success"] === true) {
                if (!function_exists("wp_handle_upload")) {
                    require_once(ABSPATH . "/wordpress/wp-admin/includes/file.php");
                }

                $file["name"] = $verifyMimetype["newName"];
                $uploadedfile = $file;
                $upload_overrides = array("test_form" => false);
                $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
            } else {
                $response = Util::response(400, "Formato de recurso invalido.", true);
                $prepared = false;
            }
        }
        if ($prepared === true) {
            $getYear = date("Y");
            $getMonth = date("m");

            $reqAddResource = array($idProperty, 1, $file["name"], "/wordpress/wp-content/uploads/".$getYear."/".$getMonth."/".$file["name"]);
            $sqlAddResource = "INSERT INTO `".DATABASE_NAME."`.`wp_property_resource` (id_property, id_type_resource, name, url_path) VALUES (?, ?, ?, ?)";
            $typesAddResource = array(PDO::PARAM_INT, PDO::PARAM_INT, PDO::PARAM_STR, PDO::PARAM_STR);

            if ($dataSource->execQuery($sqlAddResource, $typesAddResource, $reqAddResource)) {
                $response = Util::response(200, "Recurso registrado con exito.", false);
            } else {
                $response = Util::response(500, "Error al registrar Recurso.", true);
            }

            print_r($response);
        } else {
            print_r($response);
        }
    }

    public function deleteOneProperty(DataTransferProperty $dtoProperty) {
        $prepared = true;
        $response = "";
        $dataSource = new DataSource();

        $idProperty = $dtoProperty->getId();

        if (!is_numeric($idProperty)) {
            $response = Util::response(400, "Id de inmueble no es numerico.", true);
            $prepared = false;
        }
        if (count(Util::decodeJSON($this->findOneProperty($dtoProperty))) !== 1) {
            $response = Util::response(400, "Inmueble no encontrado para eliminar.", true);
            $prepared = false;
        }

        if ($prepared === true) {
            $reqDeleteProperty = array($idProperty);
            $sqlDeleteProperty = "DELETE FROM `".DATABASE_NAME."`.`wp_properties` WHERE id = ?";
            $typesDeleteProperty = array(PDO::PARAM_INT);

            if ($dataSource->execQuery($sqlDeleteProperty, $typesDeleteProperty, $reqDeleteProperty)) {
                $sqlDeletePropertyResource = "DELETE FROM `".DATABASE_NAME."`.`wp_property_resource` WHERE id_property = ?";
                if ($dataSource->execQuery($sqlDeletePropertyResource, $typesDeleteProperty, $reqDeleteProperty)) {
                    $response = Util::response(200, "Inmueble eliminado con exito.", false);  
                } else {
                    $response = Util::response(500, "Error al eliminar recursos.", true);
                }
            } else {
                $response = Util::response(500, "Error al eliminar inmueble.", true);
            }

            print_r($response);
        } else {
            print_r($response);
        }
    }

    public function findOneResource(DataTransferResource $dtoResource) {
        $dataSource = new DataSource();
        
        $idResource = $dtoResource->getId();
        $idProperty = $dtoResource->getIdProperty();
        $condition = "a.id = $idProperty AND b.id = $idResource";

        $sqlFindOneResource = "SELECT
                                    a.id AS `idProperty`,
                                    b.id AS `idResource`
                                FROM `".DATABASE_NAME."`.`wp_properties` a
                                    INNER JOIN `".DATABASE_NAME."`.`wp_property_resource` b
                                ON a.id = b.id_property
                                    WHERE $condition LIMIT 1";

        $execQuery = $dataSource->execSelectQuery($sqlFindOneResource);
        return $execQuery;
    }

    public function findDestacado(DataTransferProperty $dtoProperty, DataTransferDestacado $dtoDestacado) {
        $condition = "";
        $dataSource = new DataSource();

        $title = $dtoProperty->getTitle();
        $idDestacado = $dtoDestacado->getId();
        $position = $dtoDestacado->getPosition();
        $searchType = $dtoDestacado->getSearchType();
        $searchValue = $dtoDestacado->getSearchValue();
        $order = $dtoDestacado->getOrder();
        $limit = $dtoDestacado->getLimit();
        $offset = $dtoDestacado->getOffset();


        if ($searchType === "all" && $searchValue !== "") {
            $condition .= "a.id LIKE '%$searchValue%' OR ";
            $condition .= "a.position LIKE '%$searchValue%' OR ";
            $condition .= "c.title LIKE '%$searchValue%' OR ";
        } else if ($searchType === "particular") {
            if ($title !== "") {
                $condition .= "c.title LIKE '%$title%' OR ";
            }
            if ($idDestacado !== "") {
                $condition .= "a.id LIKE '%$idDestacado%' OR ";   
            }
            if ($position !== "") {
                $condition .= "a.position LIKE '%$position%' OR ";   
            }
        }

        if ($condition === "") {
            $condition = "1 = 1";
        } else {
            $condition = substr($condition, 0, -3);
        }

        $sqlFindAllDestacado = "SELECT SQL_CALC_FOUND_ROWS
                                    a.id AS `idDestacado`,
                                    a.position AS `posicion`,
                                    c.title AS `titulo`
                                FROM `".DATABASE_NAME."`.`wp_destacado` a
                                    INNER JOIN `".DATABASE_NAME."`.`wp_destacado_property` b
                                        ON a.id = b.id_destacado
                                    INNER JOIN `".DATABASE_NAME."`.`wp_properties` c
                                        ON b.id_property = c.id
                                WHERE $condition ORDER BY a.id $order LIMIT $limit OFFSET $offset";

        $execQuery = $dataSource->execSelectAndCountQuery($sqlFindAllDestacado);
        print_r($execQuery);
    }

    public function deleteOnePropertyDestacado(DataTransferDestacado $dtoDestacado) {
        $prepared = true;
        $condition = "";
        $findedToDelete = "";
        $dataSource = new DataSource();

        $idDestacado = $dtoDestacado->getId();
        if (!is_numeric($idDestacado)) {
            $response = Util::response(400, "Id destacado no es numerico.", true);
            $prepared = false;
        } else {
            $findedToDelete = Util::decodeJSON($this->findOnePropertyDestacado($dtoDestacado));
        }

        if (count($findedToDelete) !== 1) {
            $response = Util::response(400, "Inmueble destacado no encontrado para eliminar.", true);
            $prepared = false;
        }

        if ($prepared === true) {
            $idDestacadoProperty = $findedToDelete[0]["idPropertyDestacado"];
            $reqDeletePropertyDestacado = array($idDestacadoProperty);
            $sqlDeletePropertyDestacado = "DELETE FROM `".DATABASE_NAME."`.`wp_destacado_property` WHERE id = ?";
            $typesDeletePropertyDestacado = array(PDO::PARAM_INT);

            if ($dataSource->execQuery($sqlDeletePropertyDestacado, $typesDeletePropertyDestacado, $reqDeletePropertyDestacado)) {
                $response = Util::response(200, "Destacado eliminado con exito.", false);  
            } else {
                $response = Util::response(500, "Error al eliminar Destacado.", true);
            }

            print_r($response);
        } else {

            print_r($response);
        }
    }

    public function findOnePropertyDestacado(DataTransferDestacado $dtoDestacado) {   
        $dataSource = new DataSource();
        $id = $dtoDestacado->getId();

        if ($id !== "") {
            $condition = "b.id_destacado = $id";
        }

        $sqlFindOnePropertyDestacado = "SELECT
                                            b.id AS `idPropertyDestacado`,
                                            b.id_destacado AS `idDestacado`,
                                            b.id_property AS `idProperty` 
                                          FROM `".DATABASE_NAME."`.`wp_destacado` a
                                            INNER JOIN `".DATABASE_NAME."`.`wp_destacado_property` b
                                                ON a.id = b.`id_destacado` WHERE $condition LIMIT 1";

        $execQuery = $dataSource->execSelectQuery($sqlFindOnePropertyDestacado);
        return $execQuery;
    }

    public function addSeller(DataTransferSeller $dtoSeller) {
        $prepared = true;
        $response = "";
        $dataSource = new DataSource();

        $name = $dtoSeller->getName();
        $email = $dtoSeller->getEmail();
        $phone = $dtoSeller->getPhone();

        if (empty($name) && $name === "") {
            $response = Util::response(400, "Campo Nombre requerido.", true);
            $prepared = false;
        }
        if (empty($phone) && $phone === "") {
            $response = Util::response(400, "Campo Telefono requerido.", true);
            $prepared = false;
        } else {
            if (!is_numeric($phone)) {
                $response = Util::response(400, "Campo Telefono invalido.", true);
                $prepared = false;
            }
        }
        if (empty($email) && $email === "") {
            $response = Util::response(400, "Campo Correo requerido.", true);
            $prepared = false;
        } else {
            if (count(Util::decodeJSON($this->findOneSeller($dtoSeller))) !== 0) {
                if (Util::decodeJSON($this->findOneSeller($dtoSeller))[0]["correo"] === $email) {
                    $response = Util::response(400, "El Correo insertado ya existe.", true);
                    $prepared = false;
                }    
            }
        }
        
        if ($prepared === true) {
            $reqAddSeller = array($name, $email, $phone);
            $typesAddSeller = array(PDO::PARAM_STR, PDO::PARAM_STR, PDO::PARAM_STR);
            $sqlAddSeller = "INSERT INTO `".DATABASE_NAME."`.`wp_users_sellers` (name, email, phone) VALUES (?, ?, ?)";

            if ($dataSource->execQuery($sqlAddSeller, $typesAddSeller, $reqAddSeller)) {
                $response = Util::response(200, "Vendedor registrado con exito.", false);
            } else {
                $response = Util::response(500, "Error al registrar Vendedor.", true);
            }

            print_r($response);
        } else {

            print_r($response);
        }
    }

    public function findOneSeller(DataTransferSeller $dtoSeller) {
        $dataSource = new DataSource();

        $condition = "";
        $operation = $dtoSeller->getOps();
        $id = $dtoSeller->getId();
        $email = $dtoSeller->getEmail();

        if ($operation === "UPDATE") {
            if (!empty($id) && $id !== "") {
                $condition = "a.id = $id";
            }
        } else {
            if (!empty($email) && $email !== "") {
                $condition = "a.email = '$email'";
            }
        }

        $sqlFindOneSeller = "SELECT
                                a.id AS `idVendedor`,
                                a.name AS `nombre`,
                                a.email AS `correo`,
                                a.phone AS `telefono`,
                                a.created AS `fechaDeCreacion`
                            FROM `".DATABASE_NAME."`.`wp_users_sellers` a
                                WHERE $condition LIMIT 1";

        $execQuery = $dataSource->execSelectQuery($sqlFindOneSeller);
        return $execQuery;
    }

    public function findSeller(DataTransferSeller $dtoSeller) {
        $condition = "";
        $dataSource = new DataSource();

        $name = $dtoSeller->getName();
        $email = $dtoSeller->getEmail();
        $phone = $dtoSeller->getPhone();
        $searchType = $dtoSeller->getSearchType();
        $searchValue = $dtoSeller->getSearchValue();
        $order = $dtoSeller->getOrder();
        $limit = $dtoSeller->getLimit();
        $offset = $dtoSeller->getOffset();

        if ($searchType === "all" && $searchValue !== "") {
            $condition .= "a.name LIKE '%$searchValue%' OR ";
            $condition .= "a.email LIKE '%$searchValue%' OR ";
            $condition .= "a.phone LIKE '%$searchValue%' OR ";
        } else if ($searchType === "particular") {
            if ($name !== "") {
                $condition .= "c.name LIKE '%$name%' OR ";
            }
            if ($email !== "") {
                $condition .= "a.email LIKE '%$email%' OR ";   
            }
            if ($phone !== "") {
                $condition .= "a.phone LIKE '%$phone%' OR ";   
            }
        }

        if ($condition === "") {
            $condition = "1 = 1";
        } else {
            $condition = substr($condition, 0, -3);
        }

        $sqlFindAllSeller = "SELECT SQL_CALC_FOUND_ROWS
                                a.id AS `idVendedor`,
                                a.name AS `nombre`,
                                a.email AS `email`,
                                a.phone AS `telefono`,
                                a.created AS `fechaDeCreacion`
                            FROM `".DATABASE_NAME."`.`wp_users_sellers` a
                            WHERE $condition ORDER BY a.id $order LIMIT $limit OFFSET $offset";

        $execQuery = $dataSource->execSelectAndCountQuery($sqlFindAllSeller);
        print_r($execQuery);
    }

    public function deleteOneSeller(DataTransferSeller $dtoSeller) {
        $prepared = true;
        $condition = "";
        $findedToDelete = "";
        $dataSource = new DataSource();

        $id = $dtoSeller->getId();
        if (!is_numeric($id)) {
            $response = Util::response(400, "Id vendedor no es numerico.", true);
            $prepared = false;
        } else {
            $findedToDelete = Util::decodeJSON($this->findOneSeller($dtoSeller));
        }
        if (count($findedToDelete) !== 1) {
            $response = Util::response(400, "Id Vendedor no existe.", true);
            $prepared = false;
        }

        if ($prepared === true) {
            $idSeller = $findedToDelete[0]["idVendedor"];
            $reqDeleteSeller = array($idSeller);
            $sqlDeleteSeller = "DELETE FROM `".DATABASE_NAME."`.`wp_users_sellers` WHERE id = ?";
            $typesDeleteSeller = array(PDO::PARAM_INT);

            if ($dataSource->execQuery($sqlDeleteSeller, $typesDeleteSeller, $reqDeleteSeller)) {
                $response = Util::response(200, "Vendedor eliminado con exito.", false);  
            } else {
                $response = Util::response(500, "Error al eliminar Vendedor.", true);
            }

            print_r($response);
        } else {

            print_r($response);
        }
    }

     public function updateSeller(DataTransferSeller $dtoSeller) {
        $prepared = true;
        $response =  "";
        $condition = "";
        $findedToUpdate = "";
        $reqUpdate = array();
        $typesParamUpdate = array();
        $dataSource = new DataSource();

        $id = $dtoSeller->getId();
        $name = $dtoSeller->getName();
        $email = $dtoSeller->getEmail();
        $phone = $dtoSeller->getPhone();

        if (!is_numeric($id)) {
            $response = Util::response(400, "Id no es un numerico.", true);
            $prepared = false;
        } else {
            $findedToUpdate = Util::decodeJSON($this->findOneSeller($dtoSeller));
        }
        if (!empty($name) && $name !== "") {
            $condition .= "name = ?, ";
            array_push($reqUpdate, $name);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($phone) && $phone !== "") {
            if (is_numeric($phone)) {
                $condition .= "phone = ?, ";
                array_push($reqUpdate, $phone);
                array_push($typesParamUpdate, PDO::PARAM_STR);
            } else {
                $response = Util::response(400, "Campo Telefono invalido.", true);
                $prepared = false;
            }
        }
        if (!empty($email) && $email !== "") {
            $condition .= "email = ?, ";
            array_push($reqUpdate, $email);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (count($findedToUpdate) !== 1) {
            $response = Util::response(400, "Id Vendedor no existe.", true);
            $prepared = false;
        }

        if ($prepared === true) {
            $condition = substr($condition, 0, -2);
            $sqlUpdateSeller = "UPDATE `".DATABASE_NAME."`.`wp_users_sellers` SET $condition WHERE id = $id";

            if ($dataSource->execQuery($sqlUpdateSeller, $typesParamUpdate, $reqUpdate)) {
                $response = Util::response(200, "Vendedor actualizado con exito.", false);
            } else {
                $response = Util::response(500, "Error al actualizar Vendedor.", true);
            }

            print_r($response);
        } else {

            print_r($response);
        }
    }

    public function getSelectSellers(DataTransferSeller $dtoSeller) {
        $dataSource = new DataSource();
        $condition = "1 = 1";
        $sqlSelectSellers = "SELECT
                                a.id AS `idVendedor`,
                                a.name AS `nombre`
                            FROM `".DATABASE_NAME."`.`wp_users_sellers` a
                                WHERE $condition";
        print_r($dataSource->execSelectQuery($sqlSelectSellers));
    }

    public function updateProperty(DataTransferProperty $dtoProperty) {
        $prepared = true;
        $response =  "";
        $condition = "";
        $findedToUpdate = "";
        $reqUpdate = array();
        $typesParamUpdate = array();
        $dataSource = new DataSource();

        $id = $dtoProperty->getId();
        $title = $dtoProperty->getTitle();
        $typeProperty = $dtoProperty->getPublicationType();
        $price = $dtoProperty->getPrice();
        $address = $dtoProperty->getAddress();
        $departament = $dtoProperty->getDepartament();
        $province = $dtoProperty->getProvince();
        $district = $dtoProperty->getDistrict();
        $area = $dtoProperty->getArea();
        $latitude = $dtoProperty->getLatitude();
        $longitude = $dtoProperty->getLongitude();
        $enterprice = $dtoProperty->getEnterprice();
        $video = $dtoProperty->getVideo();
        $description = $dtoProperty->getDescription();
        $currency = $dtoProperty->getCurrency();
        $idSeller = $dtoProperty->getIdSeller();
        $propertyType = $dtoProperty->getPropertyType();

        if (!is_numeric($id)) {
            $response = Util::response(400, "Id no es un numerico.", true);
            $prepared = false;
        } else {
            $findedToUpdate = Util::decodeJSON($this->findOneProperty($dtoProperty));
        }

        if (count($findedToUpdate) !== 1) {
            $response = Util::response(400, "Id Propiedad no existe.", true);
            $prepared = false;
        }
        if (!empty($title) && $title !== "") {
            $condition .= "title = ?, ";
            array_push($reqUpdate, $title);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($typeProperty) && $typeProperty !== 0) {
            $condition .= "id_publication_type = ?, ";
            array_push($reqUpdate, $typeProperty);
            array_push($typesParamUpdate, PDO::PARAM_INT);
        }
        if (!empty($price) && $price !== "" && is_numeric($price)) {
            if (is_numeric($price)) {
                $condition .= "price = ?, ";
                array_push($reqUpdate, $price);
                array_push($typesParamUpdate, PDO::PARAM_INT);
            } else {
                $response = Util::response(400, "Campo Precio invalido.", true);
                $prepared = false;
            }
        }
        if (!empty($address) && $address !== "") {
            $condition .= "address = ?, ";
            array_push($reqUpdate, $address);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($departament) && $departament !== 0 && $departament !== "") {
            $condition .= "departament = ?, ";
            array_push($reqUpdate, $departament);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($province) && $province !== 0 && $province !== "") {
            $condition .= "province = ?, ";
            array_push($reqUpdate, $province);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($district) && $district !== 0 && $district !== "") {
            $condition .= "district = ?, ";
            array_push($reqUpdate, $district);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($area) && $area !== "") {
            $condition .= "area = ?, ";
            array_push($reqUpdate, $area);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($longitude) && $longitude !== "" && is_numeric($longitude)) {
            if (is_numeric($longitude)) {
                $condition .= "longitude = ?, ";
                array_push($reqUpdate, $longitude);
                array_push($typesParamUpdate, PDO::PARAM_STR);
            } else {
                $response = Util::response(400, "Campo Longitud invalido.", true);
                $prepared = false;
            }
        }
        if (!empty($latitude) && $latitude !== "" && is_numeric($latitude)) {
            if (is_numeric($latitude)) {
                $condition .= "latitude = ?, ";
                array_push($reqUpdate, $latitude);
                array_push($typesParamUpdate, PDO::PARAM_STR);
            } else {
                $response = Util::response(400, "Campo Latitud invalido.", true);
                $prepared = false;
            }
        }
        if (!empty($enterprice) && $enterprice !== "") {
            $condition .= "enterprice_name = ?, ";
            array_push($reqUpdate, $enterprice);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($video) && $video !== "") {
            $condition .= "url_video = ?, ";
            array_push($reqUpdate, $video);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($description) && $description !== "") {
            $condition .= "description = ?, ";
            array_push($reqUpdate, $description);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        }
        if (!empty($idSeller) && $idSeller != 0 && $idSeller !== "") {
            $condition .= "id_seller = ?, ";
            array_push($reqUpdate, $idSeller);
            array_push($typesParamUpdate, PDO::PARAM_INT);
        }
        if ($propertyType != 0) {
            $condition .= "id_property_type = ?, ";
            array_push($reqUpdate, $propertyType);
            array_push($typesParamUpdate, PDO::PARAM_INT);
        }

        if ($prepared == true) {
            $condition = substr($condition, 0, -2);
            $sqlUpdateProperty = "UPDATE `".DATABASE_NAME."`.`wp_properties` SET $condition WHERE id = $id";

            if ($dataSource->execQuery($sqlUpdateProperty, $typesParamUpdate, $reqUpdate)) {
                $response = Util::response(200, "Propiedad actualizado con exito.", false);
            } else {
                $response = Util::response(500, "Error al actualizar Propiedad.", true);
            }

            print_r($response);
            
        } else {
            print_r(json_encode($response));
        }
    }

    public function addTypeProperty(DataTransferTypeProperty $dtoTypeProperty) {
        $dataSource = new DataSource();

        $prepared = true;
        $response = "";
        $findedToCreate = "";
        $name = $dtoTypeProperty->getName();

        if (empty($name) && $name === "") {
            $response = Util::response(400, "Campo Nombre requerido.", true);
            $prepared = false;
        } else {
            $findedToCreate = Util::decodeJSON($this->findOneTypeProperty($dtoTypeProperty));
        }
        if ($findedToCreate !== "" && count($findedToCreate) === 1) {
            $response = Util::response(400, "Tipo Propiedad ya existe.", true);
            $prepared = false;
        }

        if ($prepared === true) {
            $reqAddTypeProperty = array($name);
            $typesAddTypeProperty = array(PDO::PARAM_STR);
            $sqlAddTypeProperty = "INSERT INTO `".DATABASE_NAME."`.`wp_property_types` (name) VALUES (?)";

            if ($dataSource->execQuery($sqlAddTypeProperty, $typesAddTypeProperty, $reqAddTypeProperty)) {
                $response = Util::response(200, "Tipo de Propiedad registrada con exito.", false);
            } else {
                $response = Util::response(500, "Error al registrar Tipo de Propiedad.", true);
            }

            print_r($response);
        } else {

            print_r($response);
        }
    }

    public function findOneTypeProperty(DataTransferTypeProperty $dtoTypeProperty) {
        $dataSource = new DataSource();

        $condition = "";
        $operation = $dtoTypeProperty->getOps();
        $id = $dtoTypeProperty->getId();
        $name = $dtoTypeProperty->getName();

        if ($operation === "ID") {
            if (!empty($id) && $id !== "") {
                $condition = "a.id = $id";
            }
        } else {
            if (!empty($name) && $name !== "") {
                $condition = "a.name = '$name'";
            }
        }

        $sqlFindOneTypeProperty = "SELECT
                                a.id AS `idTipoPropiedad`,
                                a.name AS `nombre`,
                                a.created AS `fechaDeCreacion`
                            FROM `".DATABASE_NAME."`.`wp_property_types` a
                                WHERE $condition LIMIT 1";

        $execQuery = $dataSource->execSelectQuery($sqlFindOneTypeProperty);
        return $execQuery;   
    }

    public function findTypeProperty(DataTransferTypeProperty $dtoTypeProperty) {
        $dataSource = new DataSource();

        $condition = "";
        $name = $dtoTypeProperty->getName();
        $searchType = $dtoTypeProperty->getSearchType();
        $searchValue = $dtoTypeProperty->getSearchValue();
        $order = $dtoTypeProperty->getOrder();
        $limit = $dtoTypeProperty->getLimit();
        $offset = $dtoTypeProperty->getOffset();

        if ($searchType === "all" && $searchValue !== "") {
            $condition .= "a.name LIKE '%$searchValue%' OR ";
        } else if ($searchType === "particular") {
            if ($name !== "") {
                $condition .= "c.name LIKE '%$name%' OR ";
            }
        }

        if ($condition === "") {
            $condition = "1 = 1";
        } else {
            $condition = substr($condition, 0, -3);
        }

        $sqlFindAllTypeProperty = "SELECT SQL_CALC_FOUND_ROWS
                                a.id AS `idTipoPropiedad`,
                                a.name AS `nombre`,
                                a.created AS `fechaDeCreacion`
                            FROM `".DATABASE_NAME."`.`wp_property_types` a
                            WHERE $condition ORDER BY a.id $order LIMIT $limit OFFSET $offset";

        $execQuery = $dataSource->execSelectAndCountQuery($sqlFindAllTypeProperty);
        print_r($execQuery);
    }

    public function updateTypeProperty(DataTransferTypeProperty $dtoTypeProperty) {
        $dataSource = new DataSource();

        $prepared = true;
        $response =  "";
        $condition = "";
        $findedToUpdate = "";
        $reqUpdate = array();
        $typesParamUpdate = array();
        

        $id = $dtoTypeProperty->getId();
        $name = $dtoTypeProperty->getName();

        if (!is_numeric($id)) {
            $response = Util::response(400, "Id no es un numerico.", true);
            $prepared = false;
        } else {
            $findedToUpdate = Util::decodeJSON($this->findOneTypeProperty($dtoTypeProperty));
        }

        if (count($findedToUpdate) !== 1) {
            $response = Util::response(400, "Id Propiedad no existe.", true);
            $prepared = false;
        }
        if (!empty($name) && $name !== "") {
            $condition .= "name = ?, ";
            array_push($reqUpdate, $name);
            array_push($typesParamUpdate, PDO::PARAM_STR);
        } 

        if ($prepared == true) {
            $condition = substr($condition, 0, -2);
            $sqlUpdateTypeProperty = "UPDATE `".DATABASE_NAME."`.`wp_property_types` a SET $condition WHERE id = $id";

            if ($dataSource->execQuery($sqlUpdateTypeProperty, $typesParamUpdate, $reqUpdate)) {
                $response = Util::response(200, "Tipo Propiedad actualizado con exito.", false);
            } else {
                $response = Util::response(500, "Error al actualizar Tipo Propiedad.", true);
            }

            print_r($response);
            
        } else {
            print_r($response);
        }
    }

    public function deleteTypeProperty(DataTransferTypeProperty $dtoTypeProperty) {
        $dataSource = new DataSource();

        $prepared = true;
        $condition = "";
        $findedToDelete = "";

        $id = $dtoTypeProperty->getId();
        if (!is_numeric($id)) {
            $response = Util::response(400, "Id Tipo Propiedad no es numerico.", true);
            $prepared = false;
        } else {
            $findedToDelete = Util::decodeJSON($this->findOneTypeProperty($dtoTypeProperty));
        }
        if (count($findedToDelete) !== 1) {
            $response = Util::response(400, "Id Tipo Propiedad no existe.", true);
            $prepared = false;
        }

        if ($prepared === true) {
            $idTypeProperty = $findedToDelete[0]["idTipoPropiedad"];
            $reqDeleteTypeProperty = array($idTypeProperty);
            $sqlDeleteTypeProperty = "DELETE FROM `".DATABASE_NAME."`.`wp_property_types` WHERE id = ?";
            $typesDeleteTypeProperty = "i";

            if ($dataSource->execQuery($sqlDeleteTypeProperty, $typesDeleteTypeProperty, $reqDeleteTypeProperty)) {
                $response = Util::response(200, "Tipo Propiedad eliminado con exito.", false);  
            } else {
                $response = Util::response(500, "Error al eliminar Tipo Propiedad.", true);
            }

            print_r($response);
        } else {

            print_r($response);
        }
    }


     public function fillSelectTypeProperty() {
        $dataSource = new DataSource();

        $condition = "1 = 1";
        $sqlSelectTypeProperty = "SELECT
                                a.id AS `idTipoPropiedad`,
                                a.name AS `nombre`
                            FROM `".DATABASE_NAME."`.`wp_property_types` a
                                WHERE $condition";
        print_r($dataSource->execSelectQuery($sqlSelectTypeProperty));
    }
    
}