<?php

class Util {

    public function __construct() {}

    public static function extractToJson($json) {
        $decode = json_decode($json, true);
        return $decode;
    }

    public static function mimetypeValid($mimetype) {
        $valid = array("jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "bmp", "BMP");
        $returnVal = false;

        for ($i = 0; $i < count($valid); $i++) {
            if ($mimetype == $valid[$i]) {
                $returnVal = true;
                break;
            }
        }

        return $returnVal;
    }

    public static function verifyMimetype($file) {
        $dataNow = date("Y-d-m-his");
        $parseFile = explode(".", $file);
        $mimetype = end($parseFile);

        if (Util::mimetypeValid($mimetype) == true) {
            $newName = "image_".$dataNow.".".$mimetype;
            $newName = strtolower($newName);
            $returnVal = array('success' => true, 'newName' => $newName);
        } else {
            $returnVal = array('success' => false, 'newName' => null );
        }

        return $returnVal;
    }

    public static function response($status, $data, $err) {
        $response = array("status" => $status, "data" => $data, "error" => $err);
        return Util::toJSON($response);
    }

    public static function toJSON($data) {
        return json_encode($data, true);
    }

    public static function decodeJSON($json) {
        return json_decode($json, true);
    }

}
