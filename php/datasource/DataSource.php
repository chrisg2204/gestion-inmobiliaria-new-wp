<?php

class DataSource
{
	private $connectString;
	private $connectInstance;

	public function __construct()
	{
		try {

			$this->connectString = "mysql:host=".DATABASE_HOST.";dbname=".DATABASE_NAME."";
			$this->connectInstance = new PDO($this->connectString, DATABASE_USERNAME, DATABASE_PASSWORD);

		} catch(PDOexception $e) {

			print_r($e->getMessage());

		}
	}

	public function __destruct()
	{
		$this->connectInstance = null;
	}

	public function getLastId()
	{
		return $this->connectInstance->lastInsertId();
	}

	public function getNumberRows()
	{
		$sql="SELECT FOUND_ROWS()";
		$statement = $this->connectInstance->prepare($sql);
		$statement->execute();

		return $statement->fetchColumn();
	}

	public function execQuery($sql, $types, $params)
	{
		$position = 1;
		if ($sql !== "") {
			if (count($params) !== 0) {
				if ($statement = $this->connectInstance->prepare($sql)) {
					for ($i = 0; $i < count($params); $i++) {
						$statement->bindParam($position, $params[$i], $types[$i]);
						$position ++;
					}

					return $statement->execute();
				} else {
					print_r($this->connectInstance->errorInfo());
				}
			} else {
				print_r("Array is empty.");
			}
		} else {
			print_r("SQL is empty.");
		}
	}

	public function execSelectAndCountQuery($sql)
	{
		if ($sql !== "") {
			if ($statement = $this->connectInstance->prepare($sql)) {
				$statement->execute();
				$numRows = $statement->rowCount();
				if ($numRows !== 0) {
					while ($file = $statement->fetch(PDO::FETCH_ASSOC)) {
						$arr[] = $file;   
					}
				} else {
					$arr = [];
				}

				return json_encode(array("total" => $this->getNumberRows(), "rows" => $arr));
			} else {
				print_r($this->connectInstance->errorInfo());
			}
		} else {
			print_r("SQL is empty.");	
		}
	}

	public function execSelectQuery($sql)
	{
		if ($sql !== "") {
			if ($statement = $this->connectInstance->prepare($sql)) {
				$statement->execute();
				$numRows = $statement->rowCount();
				if ($numRows !== 0) {
					while ($file = $statement->fetch(PDO::FETCH_ASSOC)) {
						$arr[] = $file;   
					}
				} else {
					$arr = [];
				}

				return json_encode($arr);
			} else {
				print_r($this->connectInstance->errorInfo());
			}
		} else {
			print_r("SQL is empty.");	
		}
	}

}