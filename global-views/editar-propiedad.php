<?php
/**
 * Comment Management Screen
 *
 * @package WordPress
 * @subpackage Administration
 */

/** Load WordPress Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );
require_once( dirname( __FILE__ ) . '/admin-header.php' );
require_once( dirname( __FILE__ ) . '/admin-header.php' );
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Editar Propiedad</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/bootstrap/app.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/toastr/toastr.min.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/font-awesome/font-awesome.min.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/table/bootstrap-table.min.css" /> 
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ==" crossorigin=""/>
      <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js" integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log==" crossorigin=""></script>
   </head>
   <body style="background-color:#F1F1F1">
      <div class="container">
         <div class="row" style="margin-top:1%">
            <div class="col-md-12">
               <div class="panel panel-primary">
                  <div class="panel-heading text-center">
                     <b class="panel-title">Editar Propiedad</b>
                  </div>
                  <div class="panel-body">
                     <form id="formEditProperty" name="formEditProperty" enctype="multipart/form-data" class="form-horizontal">
                            <fieldset>
                                 <div class="col-md-12" style="margin-top:1%">
                                    <label class="control-label" for="titulo">Titulo</label> 
                                    <input id="titulo" name="titulo" type="text" placeholder="Titulo" class="form-control input-md" required="" value="<?php print_r($_GET["titulo"]); ?>">
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="tipoPropiedad">Tipo de publicacion</label>
                                    <select id="tipoPropiedad" name="tipoPropiedad" class="form-control input-md">
                                       <option value="0">Seleccione...</option>
                                       <option value="1">Venta</option>
                                       <option value="2">Alquiler</option>                 
                                    </select>
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="precio">Precio</label> 
                                    <input id="precio" name="precio" type="number" min="0" class="form-control input-md" required="" value="<?php print_r(substr($_GET["precio"], 0, -2)); ?>">
                                 </div>
                                 <div class="col-md-12" style="margin-top:1%">
                                    <label class="control-label" for="direccion">Direccion</label>  
                                    <input id="direccion" name="direccion" type="text" placeholder="Direccion" class="form-control input-md" required="" value="<?php print_r($_GET["direccion"]); ?>">
                                 </div>
                                 <?php 
                                    if ($_GET["departamento"] !== 'null' && $_GET["provincia"] !== 'null' && $_GET["distrito"] !== 'null' && $_GET["area"] !== 'null') {
                                  ?>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="departamento">Departamento</label> 
                                    <select id="departamento" name="departamento" class="form-control input-md">
                                    </select>
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="provincia">Provincia</label>  
                                    <select id="provincia" name="provincia" class="form-control input-md">
                                    </select> 
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="distrito">Distrito</label>  
                                    <select id="distrito" name="distrito" class="form-control input-md">
                                    </select>
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="area">Area</label>  
                                    <input id="area" name="area" type="text" placeholder="Area" class="form-control input-md" required="" value="<?php print_r($_GET["area"]); ?>">
                                 </div>
                                 <?php 
                                       }
                                    if ($_GET["departamento"] !== 'null' && $_GET["provincia"] !== 'null' && $_GET["distrito"] !== 'null') {
                                  ?>
                                 <div class="col-md-12" style="margin-top:1%">
                                     <div id="mapid" style="height: 180px;"></div>
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="latitud">Latitud</label>  
                                    <input id="latitud" name="latitud" type="number" step="any" class="form-control input-md" value="<?php print_r($_GET["latitud"]); ?>">
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="longitud">Longitud</label>  
                                    <input id="longitud" name="longitud" type="number" step="any" class="form-control input-md" value="<?php print_r($_GET["longitud"]); ?>">
                                 </div>
                                 <?php 
                                       }
                                  ?>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="empresa">Empresa</label>  
                                    <input id="empresa" name="empresa" type="text" placeholder="Empresa" class="form-control input-md" value="<?php print_r($_GET["empresa"]); ?>">
                                 </div>
                                 <div class="col-md-12" style="margin-top:1%">
                                    <label class="control-label" for="video">Url Video</label>  
                                    <input id="video" name="video" type="text" placeholder="Url Video" class="form-control input-md" value="<?php print_r($_GET["urlVideo"]); ?>">
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="vendedor">Vendedor</label>
                                    <select id="vendedor" name="vendedor" class="form-control input-md">
                                       <option value="0">Seleccione...</option>
                                    </select> 
                                 </div>
                                 <div class="col-md-6" style="margin-top:1%">
                                    <label class="control-label" for="tipoDePropiedad">Tipo de Propiedad</label>
                                    <select id="tipoDePropiedad" name="tipoDePropiedad" class="form-control input-md">
                                       <option value="0">Seleccione...</option>
                                    </select> 
                                 </div>
                                 <div class="col-md-12" style="margin-top:1%">
                                    <label class="control-label" for="descripcion">Descripcion</label>
                                    <textarea class="form-control" id="descripcion" name="descripcion"><?php print_r($_GET["descripcion"]); ?></textarea>
                                 </div>
                             </fieldset>
                          </form>
                          <div class="col-md-12" style="padding-top: 3%; padding-bottom: 3%">
                             <div class="text-center">
                                <button id="buttonEditProperty" name="buttonEditProperty" class="btn btn-warning btn-lg">Editar <i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button>
                             </div>
                          </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/bootstrap/app.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/toastr/toastr.min.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/table/bootstrap-table.min.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/table/bootstrap-table-locale-all.min.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/config/Config.js"></script>
   <?php 
      if ($_GET["departamento"] !== 'null' && $_GET["provincia"] !== 'null' && $_GET["distrito"] !== 'null') {

    ?>
      <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/index.js"></script>
      <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/selectDependent.js"></script>

   <?php 
         }

    ?>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/editProperty.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/editar-propiedad.js"></script>
</html>

<?php


// include( dirname( __FILE__ ) . '/admin-footer.php' );
