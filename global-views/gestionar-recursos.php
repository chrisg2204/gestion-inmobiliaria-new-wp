<?php
/**
 * Comment Management Screen
 *
 * @package WordPress
 * @subpackage Administration
 */

/** Load WordPress Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );
require_once( dirname( __FILE__ ) . '/admin-header.php' );
require_once( dirname( __FILE__ ) . '/admin-header.php' );
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Gestionar Recursos</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/bootstrap/app.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/toastr/toastr.min.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/font-awesome/font-awesome.min.css" />
      <link rel="stylesheet" type="text/css" href="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/css/min/table/bootstrap-table.min.css" /> 
   </head>
   <body style="background-color:#F1F1F1">
      <div class="container">
         <div class="row" style="margin-top:1%">
            <div class="col-md-12">
               <div class="panel panel-primary">
                  <div class="panel-heading text-center">
                     <b class="panel-title">Gestionar Recursos</b>
                  </div>
                  <div class="panel-body">
                     <table
                        id="tableSpecificResources"
                        data-show-refresh="true"
                        data-page-list="[5, 10, 20, 50, 100, 200]"
                        data-pagination="true"
                        data-side-pagination="server"
                        data-show-columns="true">
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/bootstrap/app.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/toastr/toastr.min.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/table/bootstrap-table.min.js"></script>
   <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/min/table/bootstrap-table-locale-all.min.js"></script>

   <!-- List -->
      <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/gridSpecificResources.js"></script>
      <script type="text/javascript" src="/wordpress/wp-content/plugins/gestion-inmobiliaria-new-wp/public_html/assets/js/src/listSpecificResources.js"></script>
   <!-- List -->
</html>

<?php


// include( dirname( __FILE__ ) . '/admin-footer.php' );
