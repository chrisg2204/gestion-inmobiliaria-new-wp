/**
 * Toda relación va aquí.
 */

ALTER TABLE `casafly_wrdp1`.`wp_properties`
ADD CONSTRAINT `fk_publication_type` FOREIGN KEY (`id_publication_type`)
REFERENCES `casafly_wrdp1`.`wp_publication_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `casafly_wrdp1`.`wp_properties`
ADD CONSTRAINT `fk_property_type` FOREIGN KEY (`id_property_type`)
REFERENCES `casafly_wrdp1`.`wp_property_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `casafly_wrdp1`.`wp_property_resource`
ADD CONSTRAINT `fk_property` FOREIGN KEY (`id_property`)
REFERENCES `casafly_wrdp1`.`wp_properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `casafly_wrdp1`.`wp_property_resource`
ADD CONSTRAINT `fk_type_resource` FOREIGN KEY (`id_type_resource`)
REFERENCES `casafly_wrdp1`.`wp_types_resource` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `casafly_wrdp1`.`wp_destacado_property`
ADD CONSTRAINT `fk_destacado_property` FOREIGN KEY (`id_property`)
REFERENCES `casafly_wrdp1`.`wp_properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `casafly_wrdp1`.`wp_destacado_property`
ADD CONSTRAINT `fk_destacado` FOREIGN KEY (`id_destacado`)
REFERENCES `casafly_wrdp1`.`wp_destacado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `casafly_wrdp1`.`wp_properties`
ADD CONSTRAINT `fk_user_seller` FOREIGN KEY (`id_seller`)
REFERENCES `casafly_wrdp1`.`wp_users_sellers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;